export class Bank {

    constructor() {
        // Properties
        this.balance = 100
        this.outstandingLoan = 0

    }

    // Functions
    applyForLoan = (amount) => {
        if (this.outstandingLoan === 0 && Number.isInteger(amount) && amount >= 0 && amount <= this.balance * 2) {
            this.outstandingLoan = amount
            this.balance += amount
            return true
        } else {
            return false
        }
    }

    depositPayment = (amount) => {
        if (this.outstandingLoan === 0) {
            this.balance += amount
        }
        else {
            let repayment = (amount / 100) * 10

            if (this.outstandingLoan <= repayment) {
                this.balance += amount - this.outstandingLoan
                this.outstandingLoan = 0
            }
            else {
                this.balance += amount - repayment
                this.outstandingLoan -= repayment
            }
        }
    }
}
