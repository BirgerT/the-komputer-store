export class Work {

    constructor() {
        // Properties
        this.pay = 0
        this.moneyPerSecond = 0
    }

    // Functions
    doSomeWork = () => {
        this.pay += 100
    }

    earnMoneyPerSecond = () => {
        this.pay += this.moneyPerSecond
    }

    repayLoan = (amount) => {
        if (amount <= this.pay) {
            this.pay -= amount
            return 0
        }
        else {
            const rest = amount - this.pay
            this.pay = 0
            return rest
        }
    }
}
