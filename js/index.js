import { Bank } from './bank.js'
import { Work } from './work.js'
import { fetchLaptops } from './api.js'

class App {
    
    constructor() {
        // Properties
        this.bank = new Bank()
        this.work = new Work()
        this.laptops = []
        this.selectedLaptop = {}
        this.ownedLaptops = []
        
        // DOM elements
        this.elBtnGetLoan = document.getElementById('btn-get-loan')
        this.elBankBalance = document.getElementById('bank-balance-amount')

        this.elLoanPopup = document.getElementById('loan-popup')
        this.elLoanAmount = document.getElementById('loan-amount')
        this.elBtnConfirmLoan = document.getElementById('btn-confirm-loan')

        this.elLoanDisplay = document.getElementsByClassName('loan-display')
        this.elOutstandingLoan = document.getElementById('outstanding-loan')
        this.elBtnRepayLoan = document.getElementById('btn-repay-loan')

        this.elWorkPay = document.getElementById('work-pay-amount')
        this.elBtnDoWork = document.getElementById('btn-do-work')
        this.elBtnGetPayed = document.getElementById('btn-pay-to-bank')

        this.elLaptopSelector = document.getElementById('laptop-selector')
        this.elLaptopFeatures = document.getElementById('laptop-features')

        this.elLaptopDisplay = document.getElementById('laptop-display')
        this.elLaptopName = document.getElementById('laptop-name')
        this.elLaptopImage = document.getElementById('laptop-image')
        this.elLaptopDescription = document.getElementById('laptop-description')
        
        this.elLaptopPrice = document.getElementById('laptop-price')
        this.elBtnBuyLaptop = document.getElementById('btn-buy-laptop')
        this.elPurchaseMessage = document.getElementById('purchase-message')

        this.elOwnedLaptopsSection = document.getElementById('owned-laptops-section')
        this.elOwnedLaptops = document.getElementById('owned-laptops')

        // Event listeners
        this.elBtnGetLoan.addEventListener('click', this.handleGetLoan)
        this.elBtnConfirmLoan.addEventListener('click', this.handleConfirmLoan)
        this.elBtnRepayLoan.addEventListener('click', this.handleRepayLoan)
        this.elBtnDoWork.addEventListener('click', this.handleDoWork)
        this.elBtnGetPayed.addEventListener('click', this.handleGetPayed)
        this.elLaptopSelector.addEventListener('change', () => this.handleLaptopSelected(this.elLaptopSelector))
        this.elBtnBuyLaptop.addEventListener('click', this.handleBuyLaptop)

        fetchLaptops(this.laptopsFetched)
        this.renderBalance()

        setInterval(this.earnMoneyPerSecond, 1000)
    }

    // Event handlers
    handleGetLoan = () => {
        this.showLoanPopup()
    }

    handleConfirmLoan = () => {
        this.bank.applyForLoan(parseInt(this.elLoanAmount.value))
        this.hideLoanPopup()
        this.renderBalance()
    }

    handleRepayLoan = () => {
        const restLoan = this.work.repayLoan(this.bank.outstandingLoan)
        this.bank.outstandingLoan = restLoan
        this.renderPay()
        this.renderBalance()
    }

    handleDoWork = () => {
        this.work.doSomeWork()
        this.renderPay()
    }

    handleGetPayed = () => {
        this.bank.depositPayment(this.work.pay)
        this.work.pay = 0

        this.renderPay()
        this.renderBalance()
    }

    handleLaptopSelected = (selector) => {
        if (selector.value != -1) {
            const laptop = this.getLaptopById(parseInt(selector.value))
    
            this.selectedLaptop = laptop
            this.renderLaptopFeatures(laptop.specs)
            this.renderLaptopDisplay(laptop)
        }
    }

    handleBuyLaptop = () => {
        if (this.selectedLaptop.price <= this.bank.balance) {
            this.renderMessage('Congratulations on buying a new laptop.')
            this.bank.balance -= this.selectedLaptop.price
            this.work.moneyPerSecond += parseInt(this.selectedLaptop.moneyPerSecond)
            this.addToOwnedLaptops(this.selectedLaptop)
            this.renderBalance()
            this.renderOwnedLaptops()
        }
        else {
            this.renderMessage('Too expencive')
        }
    }

    // Callbacks
    laptopsFetched = (laptops) => {
        for (const laptop of laptops) {
            this.laptops.push(laptop)
        }
        this.renderLaptopOptions()
    }

    earnMoneyPerSecond = () => {
        this.work.earnMoneyPerSecond()
        this.renderPay()
    }

    // Rendering
    renderBalance = () => {
        this.elBankBalance.innerHTML = parseInt(this.bank.balance)

        if (this.bank.outstandingLoan > 0) {
            this.setAttributeOnAll('style', 'display: block', this.elLoanDisplay)
            this.elBtnRepayLoan.setAttribute('style', 'display: inline-block')
            this.elOutstandingLoan.innerHTML = parseInt(this.bank.outstandingLoan)
        } else {
            this.setAttributeOnAll('style', 'display: none', this.elLoanDisplay)
            this.elBtnRepayLoan.setAttribute('style', 'display: none')
        }
    }

    renderPay = () => {
        this.elWorkPay.innerHTML = parseInt(this.work.pay)
    }

    showLoanPopup = () => {
        this.elLoanAmount.value = ''
        this.elLoanPopup.setAttribute('style', 'display: block')
    }

    hideLoanPopup = () => {
        this.elLoanPopup.setAttribute('style', 'display: none')
    }

    renderLaptopOptions = () => {
        for (const laptop of this.laptops) {
            const elLaptop = document.createElement('option')
            elLaptop.setAttribute('value', laptop.id)
            elLaptop.innerHTML = laptop.name

            this.elLaptopSelector.appendChild(elLaptop)
        }
    }

    renderLaptopFeatures = (features) => {
        this.elLaptopFeatures.innerHTML = ''
        for (const feature of features) {
            const elFeature = document.createElement('li')
            elFeature.innerHTML = feature

            this.elLaptopFeatures.appendChild(elFeature)
        }
    }

    renderLaptopDisplay = (laptop) => {
        this.elLaptopDisplay.setAttribute('style', 'display: inline-block')

        this.elLaptopName.innerHTML = laptop.name
        this.elLaptopImage.src = laptop.image
        this.elLaptopDescription.innerHTML = laptop.description
        this.elLaptopPrice.innerHTML = laptop.price
    }

    renderOwnedLaptops = () => {
        this.elOwnedLaptops.innerHTML = ''
        this.elOwnedLaptopsSection.setAttribute('style', 'display: block')

        for (const laptopType in this.ownedLaptops) {
            if (this.ownedLaptops.hasOwnProperty(laptopType)) {
                const elLaptopType = document.createElement('div')
                elLaptopType.innerHTML = `
                    ${laptopType}: ${this.ownedLaptops[laptopType]} -
                    `
                this.elOwnedLaptops.appendChild(elLaptopType)
            }
        }
    }

    renderMessage = (message) => {
        this.elPurchaseMessage.innerHTML = message
        this.elPurchaseMessage.setAttribute('style', 'display: block')
            setTimeout(() => {
                this.elPurchaseMessage.setAttribute('style', 'display: none')
            }, 3000)
    }

    // Helper functions
    setAttributeOnAll = (attrKey, attrValue, elements) => {
        for (const element of elements) {
            element.setAttribute(attrKey, attrValue)
        }
    }

    getLaptopById = (laptopId) => {
        return this.laptops.find(laptop => laptop.id === laptopId)
    }

    addToOwnedLaptops = (laptop) => {
        if (!this.ownedLaptops.hasOwnProperty(laptop.name)) {
            this.ownedLaptops[laptop.name] = 1
        } else {
            this.ownedLaptops[laptop.name] += 1
        }
    }
}

new App()
