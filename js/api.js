export function fetchLaptops(callback) {
    fetch('http://localhost:3000/laptops')
        .then(response => response.json())
        .then(laptops => callback(laptops))
}
