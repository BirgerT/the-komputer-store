import { test } from '@jest/globals';
import { Bank } from '../js/bank.js';
import { Work } from '../js/work.js';

describe('testing the bank', () => {
    
    // Approved loans
    test('apply for loan of 200 with 100 balance and 0 loan, loan should be 200', () => {
        const bank = new Bank()
        bank.balance = 100
        bank.outstandingLoan = 0
        const expected = 200

        bank.applyForLoan(200)
        const actual = bank.outstandingLoan

        expect( actual ).toBe( expected )
    })

    test('apply for loan of 200 with 100 balance and 0 loan, balance should be 300', () => {
        const bank = new Bank()
        bank.balance = 100
        bank.outstandingLoan = 0
        const expected = 300

        bank.applyForLoan(200)
        const actual = bank.balance

        expect( actual ).toBe( expected )
    })

    test('apply for loan of 200 with 100 balance and 0 loan, should return true', () => {
        const bank = new Bank()
        bank.balance = 100
        bank.outstandingLoan = 0
        const expected = true

        const actual = bank.applyForLoan(200)

        expect( actual ).toBe( expected )
    })

    // Declined loans because requested amount is too high
    test('apply for loan of 300 with 100 balance and 0 loan, loan should be 0', () => {
        const bank = new Bank()
        bank.balance = 100
        bank.outstandingLoan = 0
        const expected = 0

        bank.applyForLoan(300)
        const actual = bank.outstandingLoan

        expect( actual ).toBe( expected )
    })

    test('apply for loan of 300 with 100 balance and 0 loan, balance should be 100', () => {
        const bank = new Bank()
        bank.balance = 100
        bank.outstandingLoan = 0
        const expected = 100

        bank.applyForLoan(300)
        const actual = bank.balance

        expect( actual ).toBe( expected )
    })

    test('apply for loan of 300 with 100 balance and 0 loan, should return false', () => {
        const bank = new Bank()
        bank.balance = 100
        bank.outstandingLoan = 0
        const expected = false

        const actual = bank.applyForLoan(300)

        expect( actual ).toBe( expected )
    })

    // Declined loans because there is an outstanding loan
    test('apply for loan of 200 with 100 balance and 100 loan, loan should be 100', () => {
        const bank = new Bank()
        bank.balance = 100
        bank.outstandingLoan = 100
        const expected = 100
       
        bank.applyForLoan(200)
        const actual = bank.outstandingLoan

        expect( actual ).toBe( expected )
    })

    test('apply for loan of 200 with 100 balance and 100 loan, balance should be 100', () => {
        const bank = new Bank()
        bank.balance = 100
        bank.outstandingLoan = 100
        const expected = 100
       
        bank.applyForLoan(200)
        const actual = bank.balance

        expect( actual ).toBe( expected )
    })

    test('apply for loan of 200 with 100 balance and 100 loan, should return false', () => {
        const bank = new Bank()
        bank.balance = 100
        bank.outstandingLoan = 100
        const expected = false

        const actual = bank.applyForLoan(200)

        expect( actual ).toBe( expected )
    })

    // Declined loans because requested amount is negative
    test('apply for loan of -100 with 0 balance and 0 loan, loan should be 0', () => {
        const bank = new Bank()
        bank.balance = 0
        bank.outstandingLoan = 0
        const expected = 0

        bank.applyForLoan(-100)
        const actual = bank.outstandingLoan

        expect( actual ).toBe( expected )
    })
    
    test('apply for loan of -100 with 0 balance and 0 loan, balance should be 0', () => {
        const bank = new Bank()
        bank.balance = 0
        bank.outstandingLoan = 0
        const expected = 0

        bank.applyForLoan(-100)
        const actual = bank.balance

        expect( actual ).toBe( expected )
    })

    test('apply for loan of -100 with 0 balance and 0 loan, should return false', () => {
        const bank = new Bank()
        bank.balance = 0
        bank.outstandingLoan = 0
        const expected = false

        const actual = bank.applyForLoan(-100)

        expect( actual ).toBe( expected )
    })

    // Declined loans because requested amount is not a number
    test('apply for loan of "a hundred" with 0 balance and 0 loan, loan should be 0', () => {
        const bank = new Bank()
        bank.balance = 0
        bank.outstandingLoan = 0
        const expected = 0

        bank.applyForLoan('a hundred')
        const actual = bank.outstandingLoan

        expect( actual ).toBe( expected )
    })
    
    test('apply for loan of "a hundred" with 0 balance and 0 loan, balance should be 0', () => {
        const bank = new Bank()
        bank.balance = 0
        bank.outstandingLoan = 0
        const expected = 0

        bank.applyForLoan('a hundred')
        const actual = bank.balance

        expect( actual ).toBe( expected )
    })

    test('apply for loan of "a hundred" with 0 balance and 0 loan, should return false', () => {
        const bank = new Bank()
        bank.balance = 0
        bank.outstandingLoan = 0
        const expected = false

        const actual = bank.applyForLoan('a hundred')

        expect( actual ).toBe( expected )
    })

    // Deposit payment with no loan
    test('deposit 200 payment with 0 balance and 0 loan, balance should be 200', () => {
        const bank = new Bank()
        bank.balance = 0
        bank.outstandingLoan = 0
        const expected = 200

        bank.depositPayment(200)
        const actual = bank.balance

        expect( actual ).toBe( expected )
    })

    test('deposit 200 payment with 123 balance and 0 loan, balance should be 323', () => {
        const bank = new Bank()
        bank.balance = 123
        bank.outstandingLoan = 0
        const expected = 323

        bank.depositPayment(200)
        const actual = bank.balance

        expect( actual ).toBe( expected )
    })

    test('deposit 200 payment with 100 balance and 0 loan, loan should be 0', () => {
        const bank = new Bank()
        bank.balance = 100
        bank.outstandingLoan = 0
        const expected = 0

        bank.depositPayment(200)
        const actual = bank.outstandingLoan

        expect( actual ).toBe( expected )
    })

    // Deposit payment with loan
    test('deposit 300 payment with 0 balance and 100 loan, balance should be 270', () => {
        const bank = new Bank()
        bank.balance = 0
        bank.outstandingLoan = 100
        const expected = 270

        bank.depositPayment(300)
        const actual = bank.balance

        expect( actual ).toBe( expected )
    })

    test('deposit 300 payment with 0 balance and 100 loan, loan should be 70', () => {
        const bank = new Bank()
        bank.balance = 0
        bank.outstandingLoan = 100
        const expected = 70

        bank.depositPayment(300)
        const actual = bank.outstandingLoan

        expect( actual ).toBe( expected )
    })

    test('deposit 400 payment with 100 balance and 25 loan, balance should be 475', () => {
        const bank = new Bank()
        bank.balance = 100
        bank.outstandingLoan = 25
        const expected = 475

        bank.depositPayment(400)
        const actual = bank.balance

        expect( actual ).toBe( expected )
    })

    test('deposit 400 payment with 100 balance and 25 loan, loan should be 0', () => {
        const bank = new Bank()
        bank.balance = 100
        bank.outstandingLoan = 25
        const expected = 0

        bank.depositPayment(400)
        const actual = bank.outstandingLoan

        expect( actual ).toBe( expected )
    })
})

describe('testing work', () => {

    // Testing do some work
    test('do some work when pay is 0, pay should be 100', () => {
        const work = new Work()
        work.pay = 0
        const expected = 100

        work.doSomeWork()
        const actual = work.pay

        expect( actual ).toBe( expected )
    })
    
    test('do some work when pay is 2345, pay should be 2445', () => {
        const work = new Work()
        work.pay = 2345
        const expected = 2445

        work.doSomeWork()
        const actual = work.pay

        expect( actual ).toBe( expected )
    })

    // Repay exact amount
    test('repay 100 in loan with 100 pay, pay should be 0', () => {
        const work = new Work()
        work.pay = 100
        const expected = 0
       
        work.repayLoan(100)
        const actual = work.pay

        expect( actual ).toBe( expected )
    })

    test('repay 100 in loan with 100 pay, should return 0', () => {
        const work = new Work()
        work.pay = 100
        const expected = 0
       
        const actual = work.repayLoan(100)

        expect( actual ).toBe( expected )
    })

    // Repay less than pay
    test('repay 100 in loan with 200 pay, pay should be 100', () => {
        const work = new Work()
        work.pay = 200
        const expected = 100
       
        work.repayLoan(100)
        const actual = work.pay

        expect( actual ).toBe( expected )
    })

    test('repay 100 in loan with 200 pay, should return 0', () => {
        const work = new Work()
        work.pay = 200
        const expected = 0
       
        const actual = work.repayLoan(100)

        expect( actual ).toBe( expected )
    })

    // Repay more than pay
    test('repay 200 in loan with 100 pay, pay should be 0', () => {
        const work = new Work()
        work.pay = 100
        const expected = 0
       
        work.repayLoan(200)
        const actual = work.pay

        expect( actual ).toBe( expected )
    })

    test('repay 200 in loan with 100 pay, should return 100', () => {
        const work = new Work()
        work.pay = 100
        const expected = 100
       
        const actual = work.repayLoan(200)

        expect( actual ).toBe( expected )
    })

    // Repay with 0 in pay
    test('repay 100 in loan with 0 pay, pay should be 0', () => {
        const work = new Work()
        work.pay = 0
        const expected = 0

        work.repayLoan(100)
        const actual = work.pay

        expect( actual ).toBe( expected)
    })

    test('repay 100 in loan with 0 pay, should return 100', () => {
        const work = new Work()
        work.pay = 0
        const expected = 100

        const actual = work.repayLoan(100)

        expect( actual ).toBe( expected)
    })
})

