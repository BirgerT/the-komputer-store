# The Komputer Store

This is my solution to assignment 1 of .Net Fullstack Upskill - Winter 2021 - Oslo.

It is a website for simulating working for money, and buying laptops for that money.



For the easiest setup of this project, follow the instructions below.

## Prerequisites

- Visual Studio Code
- Live Server extension
- Node.js

## Instructions

- Clone the repository
- Open the project in Visual Studio Code
- Run `npm start` from the projects root directory
- Open index.html in Live Server
